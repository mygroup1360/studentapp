package com.student.studentapp.service;

import java.util.List;
import com.student.studentapp.model.Student;

public interface StudentSort {
    List<Student> sortStudentsByMarks(List<Student> students);
}
