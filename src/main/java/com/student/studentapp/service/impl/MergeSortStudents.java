package com.student.studentapp.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import com.student.studentapp.model.Student;
import com.student.studentapp.service.StudentSort;

@Component
public class MergeSortStudents implements StudentSort {
    @Override
    public List<Student> sortStudentsByMarks(List<Student> students) {
        int n = students.size();
        mergeSort(students, 0, n);
        return students;
    }

    private void mergeSort(List<Student> students, int left, int right) {
        if (right - left <= 1) { return; }
        int mid = (right + left) / 2;
        mergeSort(students, left, mid);
        mergeSort(students, mid, right);
        mergeHalfs(students, left, mid, right);
    }

    private void mergeHalfs(List<Student> students, int left, int mid, int right) {
        List<Student> leftHalf = new ArrayList<>(students.subList(left, mid));
        List<Student> rightHalf = new ArrayList<>(students.subList(mid, right));
        int i = left;
        while (i != right) {
            if (leftHalf.isEmpty()) {
                while (!rightHalf.isEmpty()) {
                    students.set(i++, rightHalf.remove(0));
                }
            }
            else if (rightHalf.isEmpty()) {
                while (!leftHalf.isEmpty()) {
                    students.set(i++, leftHalf.remove(0));
                }
            }
            else {
                if (leftHalf.get(0).compareTo(rightHalf.get(0)) > 0) {
                    students.set(i++, leftHalf.remove(0));
                }
                else {
                    students.set(i++, rightHalf.remove(0));
                }
            }
        }
    }
}
