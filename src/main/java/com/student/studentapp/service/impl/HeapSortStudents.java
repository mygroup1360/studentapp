package com.student.studentapp.service.impl;

import java.util.List;
import org.springframework.stereotype.Component;
import com.student.studentapp.model.Student;
import com.student.studentapp.service.StudentSort;

@Component
public class HeapSortStudents implements StudentSort {

    @Override
    public List<Student> sortStudentsByMarks(List<Student> students) {
        createMinHeap(students);
        heapSort(students, students.size());
        return students;
    }

    private void heapSort(List<Student> students, int n) {
        while (n > 1) {
            changeIndexes(students, 0, --n);
            heapify(students, 0, n);
        }
    }

    private void createMinHeap(List<Student> students) {
        int lastNonLeafIndex = students.size() / 2 - 1;
        for (int i = lastNonLeafIndex; i >= 0; i--) {
            heapify(students, i, students.size());
        }
    }

    private void heapify(List<Student> students, int i, int n) {
        if (n > 2 * i + 1) {
            int minLeafIndex = i * 2 + 1;
            if (n > minLeafIndex + 1) {
                minLeafIndex = students.get(minLeafIndex).compareTo(students.get(minLeafIndex + 1)) < 0 ? minLeafIndex : minLeafIndex + 1;
            }
            if (students.get(i).compareTo(students.get(minLeafIndex)) > 0) {
                changeIndexes(students, i, minLeafIndex);
                if (2 * minLeafIndex + 1 < n) {
                    heapify(students, minLeafIndex, n);
                }
            }
        }
    }

    private void changeIndexes(List<Student> students, int j, int i) {
        Student student = students.get(j);
        students.set(j, students.get(i));
        students.set(i, student);
    }
}
