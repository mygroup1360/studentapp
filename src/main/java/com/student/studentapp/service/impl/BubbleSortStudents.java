package com.student.studentapp.service.impl;

import java.util.List;
import org.springframework.stereotype.Component;
import com.student.studentapp.model.Student;
import com.student.studentapp.service.StudentSort;

@Component
public class BubbleSortStudents implements StudentSort {
    @Override
    public List<Student> sortStudentsByMarks(List<Student> students) {
        for (int i = 0; i < students.size() - 1; i++) {
            for (int j = 0; j < students.size() - 1 - i; j++) {
                if (students.get(j).compareTo(students.get(j + 1)) < 0) {
                    changeIndexes(students, j, j + 1);
                }
            }
        }
        return students;
    }

    private void changeIndexes(List<Student> students, int j, int i) {
        Student student = students.get(j);
        students.set(j, students.get(i));
        students.set(i, student);
    }
}
