package com.student.studentapp.service.impl;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import com.student.studentapp.model.Student;
import com.student.studentapp.repository.StudentRepository;
import com.student.studentapp.service.StudentService;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public String addStudent(Student student) {
        student.setId(new ObjectId().toString());
        studentRepository.save(student);
        return student.getId();
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }
}
