package com.student.studentapp.service;

import java.util.List;
import com.student.studentapp.model.Student;

public interface StudentService {
    String addStudent(Student student);

    List<Student> getAllStudents();
}
