package com.student.studentapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.student.studentapp.model.Student;
import com.student.studentapp.model.StudentListDTO;
import com.student.studentapp.service.StudentService;
import com.student.studentapp.service.impl.BubbleSortStudents;
import com.student.studentapp.service.impl.HeapSortStudents;
import com.student.studentapp.service.impl.MergeSortStudents;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping(value = "/students")
public class WebController {

    private final StudentService studentService;
    private final BubbleSortStudents bubbleSortStudents;
    private final MergeSortStudents mergeSortStudents;
    private final HeapSortStudents heapSortStudents;

    @GetMapping
    public String index(Model model) {

        StudentListDTO studentListDTO = StudentListDTO.builder().students(studentService.getAllStudents()).build();
        model.addAttribute("studentsList", studentListDTO);
        return "students";
    }

    @GetMapping("new_student")
    public String newStudent(Model model) {
        model.addAttribute("student", Student.builder().build());
        return "new_student";
    }

    @PostMapping("save")
    public String saveStudent(@ModelAttribute("student") Student student) {
        studentService.addStudent(student);
        return "redirect:/students";
    }

    @PostMapping("bubble")
    public String bubbleSort(@ModelAttribute("studentsList") StudentListDTO studentList, Model model) {
        long start = System.nanoTime();
        bubbleSortStudents.sortStudentsByMarks(studentList.getStudents());
        long finish = System.nanoTime();
        model.addAttribute("studentsList", studentList);
        model.addAttribute("executionMessage", "Bubble sort executed in " + (finish - start) + " ns");
        return "students";
    }

    @PostMapping("merge")
    public String mergeSort(@ModelAttribute("studentsList") StudentListDTO studentList, Model model) {
        long start = System.nanoTime();
        mergeSortStudents.sortStudentsByMarks(studentList.getStudents());
        long finish = System.nanoTime();
        model.addAttribute("studentsList", studentList);
        model.addAttribute("executionMessage", "Merge sort executed in " + (finish - start) + " ns");
        return "students";
    }

    @PostMapping("heap")
    public String heapSort(@ModelAttribute("studentsList") StudentListDTO studentList, Model model) {
        long start = System.nanoTime();
        heapSortStudents.sortStudentsByMarks(studentList.getStudents());
        long finish = System.nanoTime();
        model.addAttribute("studentsList", studentList);
        model.addAttribute("executionMessage", "Heap sort executed in " + (finish - start) + " ns");
        return "students";
    }
}
