package com.student.studentapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Student implements Comparable<Student> {
    private String id;
    private String name;
    private double mark;

    @Override
    public int compareTo(Student o) {
        if (this.mark > o.getMark()) {
            return 1;
        }
        else if (this.mark == o.getMark()) {
            return 0;
        }
        else {
            return -1;
        }
    }
}
