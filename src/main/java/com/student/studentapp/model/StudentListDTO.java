package com.student.studentapp.model;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentListDTO {
    private List<Student> students;

    public void addStudent(Student student) {
        students.add(student);
    }
}
