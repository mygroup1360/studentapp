package com.student.studentapp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.student.studentapp.model.Student;

public interface StudentRepository extends MongoRepository<Student, String> {
}
